package com.shoeboxed

import java.io.File
import java.io.FileFilter
import scala.io.Source
import org.deeplearning4j.text.sentenceiterator.BaseSentenceIterator
import scala.annotation.tailrec
import java.io.BufferedReader
import java.io.FileReader
import java.io.StringWriter
import java.util.ArrayList
import scala.collection.JavaConversions._
import scala.collection.mutable.MutableList
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor

case class TokenTuple(value: String) {
  lazy val values = value.split(" ")
  def token = values(0)
  def pos = values(1)
  def iob = values(2)
}

class CorpusIterator(path: String) extends BaseSentenceIterator {
  var iterator = Corpus.fromPathEager(path)
  override def nextSentence = iterator.next.map(_.split(" ")(0)).mkString(" ")
  override def hasNext = iterator.hasNext
  override def reset = { iterator = Corpus.fromPathEager(path) }
}

object Corpus {

  val LineSeparator = System.getProperty("line.separator")

  def lines(f: File) = {
    val res = Source.fromFile(f).getLines
    //    println
    //    println(" -- Loading " + f.getName)
    //    println
    res
  }

  def readFile(f: File): Seq[Seq[String]] = {
    //println(" -- Loading " + f.getName)
    val lst = new MutableList[MutableList[String]]
    val currentSentence = new MutableList[String]
    Source.fromFile(f).getLines.foreach { line =>
      if (line.trim != "") {
        currentSentence += line
      } else {
        lst += MutableList() ++ currentSentence
        currentSentence.clear()
      }
    }
    lst.toList //back to immutable
  }

  def readFile2(f: File): Seq[Seq[String]] = {
    println(" -- Loading " + f.getName)
    val lst = new ArrayList[ArrayList[String]]
    var sentence = new ArrayList[String]()
    Source.fromFile(f).getLines.foreach { line =>
      if (line.trim != "") {
        sentence.add(line)
      } else {
        lst.add(sentence)
        sentence = new ArrayList[String]()
      }
    }
    lst.map(_.toList)
  }

  def fromPathEager(path: String) = {
    val file = new File(path)
    if (!file.exists())
      throw new IllegalArgumentException(s"File $path doesn't exist!")

    val files = file.isDirectory match {
      case false => Array(file)
      case true =>
        file.listFiles(new FileFilter {
          override def accept(file: File) = file.getName.endsWith(".txt")
        })
    }

    println(" -- Loading corpus files: " + files.size)
    files.iterator.flatMap(readFile).filter(_.size > 0)
  }

  def fromPath(path: String) = {
    val file = new File(path)
    if (!file.exists())
      throw new IllegalArgumentException(s"File $path doesn't exist!")

    val files = file.isDirectory match {
      case false => Array(file)
      case true =>
        file.listFiles(new FileFilter {
          override def accept(file: File) = file.getName.endsWith(".txt")
        })
    }

    println(" -- Loading corpus files: " + files.size)
    val sents = files.iterator.flatMap(lines)

    var count = 0
    def recur(lst: Iterator[String]): Iterator[Seq[TokenTuple]] = {
      count += 1
      val t = System.currentTimeMillis
      val pair = lst.span(_.trim != "") //first empty trim means a single newline
      val firstLine = pair._1.toList.map(TokenTuple(_))
      if (pair._2.isEmpty) {
        print("\n -- stopping because empty at " + firstLine + "\n")
        println(s" -- Loop $count took ${System.currentTimeMillis - t} millis")
        Seq(firstLine).iterator
      } else {
        pair._2.next //just skip over the new line where we stopped the prev iteration
        println(s" -- Loop $count took ${System.currentTimeMillis - t} millis")
        Seq(firstLine).iterator ++ recur(pair._2)
      }
    }
    recur(sents)
  }

  def main(args: Array[String]): Unit = {
    val dir = "/Users/Dorel/Work/shoeboxed/py-classifier/corpus_ner_big/all-/training"
    var c = 0
    val t = System.currentTimeMillis;
    Corpus.fromPathEager(dir).foreach { x => c += 1; x.map(x => x.split(" ")(0)).mkString(" "); println(c + " ") }
    println(s" -- Finished in ${System.currentTimeMillis - t} millis")
  }

}