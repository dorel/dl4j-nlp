package com.shoeboxed

import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator
import scala.io.Source
import java.io.File
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory
import org.deeplearning4j.models.word2vec.Word2Vec
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess
import scala.collection.JavaConversions._
import org.deeplearning4j.plot.Tsne

object w2v {

  def go = {

    val dir = "/Users/Dorel/Work/shoeboxed/py-classifier/corpus_ner_big/square-/training"
    /*
     * !!! BIG WARNING HERE !!!
     * Do not set a preprocessor on this iterator, it never gets called.
     * I'm too lousy of a programmer to understand why.
     * Instead, pre-process them directly in the iterator, when reading them.
     */
    val iter = new CorpusIterator(dir)
    val t = new DefaultTokenizerFactory

    val w2vModelvec =
      new Word2Vec.Builder()
        .sampling(1e-5)
        .minWordFrequency(0)
        .batchSize(1000)
        .useAdaGrad(false)
        .layerSize(100)
        .iterations(3)
        .learningRate(0.025)
        .minLearningRate(1e-2)
        .negativeSample(10)
        .iterate(iter)
        .tokenizerFactory(t)
        .build
    w2vModelvec.fit()
    w2vModelvec
  }

  def main(args: Array[String]): Unit = {
    val w2vModel = go
    println(" -- Plotting t-SNE...")
    val tsne = new Tsne.Builder()
      .setMaxIter(200)
      .learningRate(500)
      .useAdaGrad(false)
      .normalize(false)
      .usePca(false)
      .build
    w2vModel.lookupTable().plotVocab(tsne)
    println(w2vModel.getWordVector("Amazon").mkString(" "))
    println(w2vModel.getWordVector("Eventbrite").mkString(" "))
  }

}