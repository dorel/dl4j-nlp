name := "sbx-dl4j"

organization := "com.shoeboxed"

version := "0.0.1"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

scalaVersion := "2.10.4"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

javaOptions += "-Xmx12g"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

libraryDependencies ++= {
  val akkaV = "2.3.9"
  val sprayV = "1.3.3"
  val sparkV = "1.3.1"
  val dl4jV = "0.0.3.3.4.alpha1-SNAPSHOT"
  val nd4jV = "0.0.3.5.5.4"
  val canovaV = "0.0.0.2"

  Seq(
    "io.spray"            %%  "spray-can"     		% sprayV,
    "io.spray"            %%  "spray-routing" 		% sprayV,
    "com.typesafe.akka"   %%  "akka-actor"    		% akkaV,
    "org.apache.spark" 	   %  "spark-core_2.10" 	% sparkV,
    "org.apache.spark" 	   %  "spark-mllib_2.10"  	% sparkV,
    "org.deeplearning4j"   %  "deeplearning4j-ui"       % dl4jV,
    "org.deeplearning4j"   %  "deeplearning4j-nlp"      % dl4jV,
    "org.nd4j"             %  "nd4j-jblas"              % nd4jV,
    "org.nd4j"             %  "nd4j-api"                % nd4jV
    //"org.nd4j"             %  "nd4j-jcublas-7.0"        % nd4jV
  )
}

Revolver.settings

//EclipseKeys.withSource := true
